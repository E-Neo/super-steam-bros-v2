﻿using UnityEngine;
using System.Collections;

public class Switch : MonoBehaviour {

    public bool FireCheck; //true if the switch can be activated by fire
    public bool WaterCheck; //true if the switch can be activated by water
    public bool SteamCheck; //true if the switch can be activated by steam
    public bool Activated; //true if switch has been activated
    public bool OneWay; //true if the switch can't be deactivated
    private bool Done;
    public GameObject Reciever;

    //comment

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if ((Activated == true) && (Done == false))
        {
            Reciever.SendMessage("Action");
            Done = true;
        }
	}

    void SwitchCheck (string type)
    {
        if (((OneWay == true) && (Activated == false)) || (OneWay == false))
        {
            if (FireCheck == true)
		    {
			    if (type == "Fire")
			    {
                    Activated = !Activated;
			    }
		    }else if (WaterCheck == true)
		    {
			    if (type == "Water")
			    {
				    Activated = !Activated;
			    }
		    }else if (SteamCheck == true)
		    {
    			if (type == "Steam")
	    		{
		    		Activated = !Activated;
			    }
    		} 	
        }
    }
}
