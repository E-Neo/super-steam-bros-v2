﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

    public float speed = 5.0f;
    private Vector3 dir;
    public Rigidbody rgdbdy;
    private float Val;
    public float JumpForce = 400;
    private int DoubleJump = 2;
    public Transform trnsfrm;
    public bool POne;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (POne == true)
        {
            //print(Input.GetAxis("Fire Vert"));
                
            //rgdbdy.AddForce(speed * transform.right * Input.GetAxis("Horizontal"));
            transform.position += (speed * Input.GetAxis("Horizontal P1") * transform.right * Time.deltaTime);
            if (Input.GetButtonDown("Jump1"))
            {
                if (DoubleJump > 0)
                rgdbdy.AddForce(transform.up * JumpForce);
                DoubleJump -= 1;
            }


        }
        else if (POne == false)
        {
            //print(Input.GetAxis("Fire Vert"));

            //rgdbdy.AddForce(speed * transform.right * Input.GetAxis("Horizontal"));
            transform.position += (speed * Input.GetAxis("Horizontal P2") * transform.right * Time.deltaTime);
            if (Input.GetButtonDown("Jump2"))
            {
                if (DoubleJump > 0)
                    rgdbdy.AddForce(transform.up * JumpForce);
                DoubleJump -= 1;
            }


        }

        //Val = Mathf.Sqrt(Mathf.Pow(Input.GetAxis("Fire Vert"), 2) + Mathf.Pow(Input.GetAxis("Fire Hori"), 2));

        //Val = Mathf.Abs(Input.GetAxis("Vertical") + Input.GetAxis("Horizontal"));

        //if (Mathf.Abs(Input.GetAxis("Fire Hori")) > 0 || Mathf.Abs(Input.GetAxis("Fire Vert")) > 0)
        //{

        //    dir = new Vector3(Input.GetAxis("Fire Hori"), -1 * Input.GetAxis("Fire Vert"), 0);
        //    Ray ray = Camera.main.ScreenPointToRay(dir * 50);
        //    print(dir);
        //    RaycastHit hit;

        //    Debug.DrawLine(trnsfrm.position, dir * 10, Color.red);
        //    if (Physics.Raycast(trnsfrm.position, dir * 10, out hit, 10))
        //    {
        //        print(hit.transform.tag);
        //        Debug.DrawRay(trnsfrm.position, dir * 10, Color.green);
        //        Debug.DrawLine(trnsfrm.position, hit.point, Color.green);
        //    }
        //}
	}

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            DoubleJump = 2;
        }
        if (collision.gameObject.tag == "Roof")
        {
                DoubleJump = 0;
        }
    }

}
