﻿using UnityEngine;
using System.Collections;

public class BeamSwitcher : MonoBehaviour {

    public string beamType;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Switch")
        {
            other.gameObject.SendMessage("SwitchCheck", beamType);
        }
    }
}
