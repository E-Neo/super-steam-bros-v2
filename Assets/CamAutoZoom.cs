﻿using UnityEngine;
using System.Collections;

public class CamAutoZoom : MonoBehaviour {

    public Transform m_Start;
    public Transform m_End;
    public char AxiesMode;
    public Transform PlayerA;
    public Transform PlayerB;
    public float minXToZoom;
    public float maxZToStop;
    private float XDistPlayers;
    private Vector3 tempDist;

    [Range(0, 1)]
    public float step = 0.0f;

    private Vector3 pos;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        tempDist = PlayerA.position - PlayerB.position;
        if (AxiesMode == 'Z') //Zoom
        {
            //XDistPlayers = Mathf.Abs(tempDist.x);
            XDistPlayers = Mathf.Sqrt((Mathf.Pow(tempDist.x, 2)) + (Mathf.Pow(tempDist.y, 2)));
        }
        else if (AxiesMode == 'Y') //Scroll Up
        {
            XDistPlayers = (Mathf.Abs(tempDist.y))/2;
        }
        else if (AxiesMode == 'X') //Scroll Up
        {
            XDistPlayers = (Mathf.Abs(tempDist.x)) / 2;

        }

        if (XDistPlayers > minXToZoom)
        {
             if (XDistPlayers < maxZToStop)
             {
                    step = (XDistPlayers / maxZToStop);
             }
        }



        pos = Vector3.Lerp(m_Start.position, m_End.position, step);

        transform.position = pos;

    }
}
