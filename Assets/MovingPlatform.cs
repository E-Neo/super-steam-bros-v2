﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {

    public Transform m_Start;
    public Transform m_End;

    private Vector3 pos;

    [Range(0, 1)]
    public float step = 0.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        pos = Vector3.Lerp(m_Start.position, m_End.position, step);

        transform.position = pos;
	}
}
