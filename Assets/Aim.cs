﻿using UnityEngine;
using System.Collections;

public class Aim : MonoBehaviour {

    public GameObject[] Rets = new GameObject[5];
    private Vector3 AimDir;
    private float AimMag;
    public Transform Player;
    public bool PTwo;
    public GameObject Beam;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (PTwo == false)
        {
            if (Mathf.Abs(Input.GetAxis("Fire Hori Test P1")) > 0 || Mathf.Abs(Input.GetAxis("Fire Vert Test P1")) > 0)
            {
                AimDir = new Vector3(Input.GetAxis("Fire Hori Test P1"), -1 * Input.GetAxis("Fire Vert Test P1"), 0);
                //AimMag = Mathf.Sqrt(Mathf.Pow(Input.GetAxis("Fire Hori Test"), 2) + Mathf.Pow(Input.GetAxis("Fire Vert Test"), 2));
                //AimMag = Mathf.Sqrt(Mathf.Pow(Input.GetAxis("Fire Hori Test"), 2) + Mathf.Pow(Input.GetAxis("Fire Vert Test"), 2));
                //AimMag = (10 * AimMag) / 5;
                AimMag = 2;

                for (int i = 0; i < 5; i++)
                {
                    Rets[i].active = true;
                    Rets[i].transform.position = Player.position + (AimDir * AimMag * (i + 1));
                }
            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    Rets[i].active = false;
                }
            }

            //if (Input.GetButton("Fire1"))
            if (((Input.GetButton("Fire1")) && (Mathf.Abs(Input.GetAxis("Fire Hori Test P1")) > 0)) || ((Input.GetButton("Fire1")) && (Mathf.Abs(Input.GetAxis("Fire Hori Test P1")) > 0)))
            {
                if (Beam.active == false)
                {
                    Beam.active = true;
                }
                float tempAngle;
                tempAngle = (Vector3.Angle(Vector3.up, AimDir)) * (Mathf.Sign(Vector3.Dot(Vector3.forward, Vector3.Cross(Vector3.up, AimDir))));
                Beam.transform.rotation = Quaternion.Euler(new Vector3(Beam.transform.rotation.x, Beam.transform.rotation.y, tempAngle));

            }
            //if (Input.GetButtonUp("Fire1"))
            if ((Input.GetButtonUp("Fire1")) && (Beam.active == true))
            {
                Beam.active = false;

            }

        }
        else
        {
            if (Mathf.Abs(Input.GetAxis("Fire Hori Test P2")) > 0 || Mathf.Abs(Input.GetAxis("Fire Vert Test P2")) > 0)
            {
                AimDir = new Vector3(Input.GetAxis("Fire Hori Test P2"), -1 * Input.GetAxis("Fire Vert Test P2"), 0);
                //AimMag = Mathf.Sqrt(Mathf.Pow(Input.GetAxis("Fire Hori Test"), 2) + Mathf.Pow(Input.GetAxis("Fire Vert Test"), 2));
                //AimMag = Mathf.Sqrt(Mathf.Pow(Input.GetAxis("Fire Hori Test"), 2) + Mathf.Pow(Input.GetAxis("Fire Vert Test"), 2));
                //AimMag = (10 * AimMag) / 5;
                AimMag = 2;

                for (int i = 0; i < 5; i++)
                {
                    Rets[i].active = true;
                    Rets[i].transform.position = Player.position + (AimDir * AimMag * (i + 1));
                }
            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    Rets[i].active = false;
                }
            }

            //if (Input.GetButton("Fire2"))
            if (((Input.GetButton("Fire2")) && (Mathf.Abs(Input.GetAxis("Fire Hori Test P2")) > 0)) || ((Input.GetButton("Fire2")) && (Mathf.Abs(Input.GetAxis("Fire Hori Test P2")) > 0)))
            {
                if (Beam.active == false)
                {
                    Beam.active = true;
                }
                float tempAngle;
                tempAngle = (Vector3.Angle(Vector3.up, AimDir)) * (Mathf.Sign(Vector3.Dot(Vector3.forward, Vector3.Cross(Vector3.up, AimDir))));
                Beam.transform.rotation = Quaternion.Euler(new Vector3(Beam.transform.rotation.x, Beam.transform.rotation.y, tempAngle));
            }
            //if (Input.GetButtonUp("Fire2"))
            if ((Input.GetButtonUp("Fire2")) && (Beam.active == true))
            {
                Beam.active = false;

            }
        }


	}


}
